$(document).ready(function() {
    var arrayCommitsAmount = [];
    var commitsAmount = $('.allData').each( function() {
        var currentItem = [];
        currentItem.push($(this).data('authorName'));
        currentItem.push($(this).data('commitsAmount'));
        arrayCommitsAmount.push(currentItem);
    })

    console.log(arrayCommitsAmount);
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Statistics and information about user repository'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b style="font:bold 26px Arial">{point.name}</b> - <span style="font: 14px Arial">{point.percentage:.1f} % </span>'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            data: arrayCommitsAmount
        }]
    });
});

