<?php

namespace GitParser\ParserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('info', array()))
            ->add('url', 'text', array(
                    'attr'   =>  array(
                        'placeholder' => 'Url',
                    )
                ))
            ->add('private','password', array(
                    'attr'   =>  array(
                        'placeholder' => 'Private Key',
                    ),
                    'required' => false,
                ))
            ->add('parse', 'submit', array(
                    'attr'   =>  array(
                        'class' => 'btn btn-primary btn-large',
                    ),
                ))
            ->setMethod('GET')
            ->getForm();

        return $this->render('ParserBundle:Default:index.html.twig', array(
                'form' => $form->createView(),
            ));
    }

    /**
     */
    public function infoAction()
    {
        set_time_limit($this->container->getParameter('time_limit'));
        $formData = $this->getRequest()->get('form');
        $parseRequest = new \GitParser\ParserBundle\Entity\ParseRequest($this->container, $formData['url'],
            $formData['private']);
        $stats = $parseRequest->parse();
        return $this->render('ParserBundle:Default:info.html.twig', array(
            'authors_info' => $stats['authors_by_commits'],
            'commits_amount' => $stats['commits_amount'],
        ));
    }
}
